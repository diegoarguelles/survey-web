import {HttpClient} from 'aurelia-fetch-client';

export class Home {

  surveys = []

  httpClient = new HttpClient();

  activate() {
    this.httpClient.fetch('https://tcc-grupo5.herokuapp.com/api/questionnaire/evaluator/20621695')
      .then(response => response.json())
      .then(response => {
        console.log(response)
        this.surveys = response;
      });
  }


  goToSurvey(id) {

    window.location.href = '/#/survey/'+id;
    window.location.reload();
  }

}
