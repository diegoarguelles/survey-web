import {HttpClient} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';

import {MdToastService} from 'aurelia-materialize-bridge';

@inject(MdToastService)
export class TakeSurvey {

  constructor(toast) {
    this.toast = toast;
  }

  httpClient = new HttpClient();

  survey = {}

  surveyAnswer = {
    evaluator: 'Diego Arguelles',
    evaluated: 'Alexiz Zambrano',
    questionnaireType: 'Docente',
    questions: []
  }

  activate(params) {
    this.httpClient.fetch('https://tcc-grupo5.herokuapp.com/api/questionnaire/' + params.idsurvey)
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.survey = response;
      });
  }

  saveSurvey() {
    /*this.survey.questions.map(question => {
      let answer = {
        id: question._id,
        text: question.text,
        questionType: question.questionType,
        answer: this.getAnswerValue(question._id, question.questionType)
      }
      console.log(answer);
    });*/
    this.toast.show('Saved!!!', 4000)
    window.location.href = '/#/home';
    window.location.reload();
  }

  getAnswerValue(questionId, questionType) {
    let answer = ''
    if (questionType === 'text') {
      answer = document.getElementById('text' + questionId).value;
    } else if (questionType === 'checkbox') {
      console.log('Checkbox')
      let checkedValue = document.querySelector('.checkbox' + questionId).value;
      answer = checkedValue;
    }
    console.log(answer);
  }

}
