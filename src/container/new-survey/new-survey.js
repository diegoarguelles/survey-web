import {computedFrom, inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import {MdToastService} from 'aurelia-materialize-bridge';

@inject(MdToastService)
export class NewSurvey {

  constructor(toast) {
    this.toast = toast;
  }

  selectedDate = null;

  setDate() {
    let date = new Date();
    this.selectedDate = date;
  }

  owner = 'Diego Arguelles';
  type = '';

  questions = [];
  labelValue;
  fileInput;
  isManualCreation = false;
  answerOptions = [];
  currentAnswerOption;
  answerType = '';
  showRadioOptions = false;
  showCheckboxOptions = false;
  showInputOptionText = false;
  textQuestion = '';
  evaluated = '';
  dateInit = '';
  dateEnd = '';

  showWizard = true;
  cancelBackLabel = 'Cancel';

  httpClient = new HttpClient();


  @computedFrom('fileInput.files')
  get selectedFile() {
    if (this.fileInput.files) {
      console.log(this.fileInput.files);
      return this.fileInput.files.length > 0 ? this.fileInput.files[0] : '';
    }
  }

  markAnswerType() {
    if (this.answerType === 'text') {
      this.showInputOptionText = false;
    }
    else if (this.answerType === 'radio') {
      this.showInputOptionText = true;
      this.showRadioOptions = true;
      this.showCheckboxOptions = false;
    }
    else if (this.answerType === 'checkbox') {
      this.showInputOptionText = true;
      this.showRadioOptions = false;
      this.showCheckboxOptions = true;
    }
  }

  switchToManually() {
    this.isManualCreation = true;
    this.resetForm();
    this.showWizard = true;
  }

  returnToMainOptions() {
    this.isManualCreation = false;
    this.resetForm();
  }

  addAnswerOption() {
    this.answerOptions.push(this.currentAnswerOption);
    this.currentAnswerOption = '';
  }

  addQuestion() {

    if (this.answerType === '') {
      // TODO show toast
      return false;
    }

    this.questions.push(
      {
        text: this.textQuestion,
        questionType: this.answerType,
        options: this.answerOptions
      }
    );

    this.resetForm();
  }

  saveSurvey() {

    let survey = {
      evaluated: this.evaluated,
      questionnaireType: this.type,
      dateInit: this.dateInit,
      dateEnd: this.dateEnd,
      questions: this.questions
    };

    this.httpClient.fetch('https://tcc-grupo5.herokuapp.com/api/questionnaire', {
      method: 'post',
      body: json(survey)
    }).then(response => response.json())
      .then(response => {
        this.toast.show('Survey created!', 4000)
        this.blockForm();
        this.cancelBackLabel = 'Back';
      });
  }

  blockForm() {
    this.showWizard = false;
    this.resetForm();
  }

  resetForm() {
    this.textQuestion = '';
    this.answerType = '';
    this.answerOptions = [];
    this.showRadioOptions = false;
    this.showCheckboxOptions = false;
    this.showInputOptionText = false;
  }

}
