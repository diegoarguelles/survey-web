export class App {
  constructor() {
    this.message = 'Hello World!';
  }

  configureRouter(config, router) {
    this.router = router;
    config.title = 'Survey';
    config.map([
      { route: ['', 'login'], name: 'login', moduleId: PLATFORM.moduleName('container/login/login'), title: 'Login' },
      { route: 'home', name: 'home', moduleId: PLATFORM.moduleName('container/home/home'), title: 'Home' },
      { route: 'survey', name: 'newSurvey', moduleId: PLATFORM.moduleName('container/new-survey/new-survey'), title: 'New survey', nav: true },
      { route: 'survey/:idsurvey', name: 'takeSurvey', moduleId: PLATFORM.moduleName('container/take-survey/take-survey'), title: 'Take survey', nav: false }
    ]);
  }

}
